$(document).ready(function(){

  $('.form-login').validate({
      rules: {
        username:{
          required: true
        },
        password:{
          required: true
        }
      },
      messages: {
        username:{
          required: 'Ingrese usuario.'
        },
        password:{
          required: 'Ingrese contraseña.'
        }
      },
      submitHandler: function(form) {
        form.submit();
      },
      invalidHandler: function(e, validator) {
        // $(".box-error").css('display','block').html("Debes completar o ingresar los datos correctos en los siguientes campos");
      },
      errorLabelContainer: $(".errorHolder")
      /*errorPlacement: function(error, element) {
        error.insertBefore(element);
      }*/
    });
//=========================================================================================== 
    $('.form-search').validate({
      rules: {
        dni:{
          required: true
        },
        tipo_doc:{
          required: true
        }
      },
      messages: {
        dni:{
          required: 'Número de documento es requerido.'
        },
        tipo_doc:{
          required: 'Tipo de documento es requerido.'
        }
      },
      submitHandler: function(form) {
        form.submit();
      },
      invalidHandler: function(e, validator) {
        //$(".errorHolder").html("<label class='error'>Todos los campos son requeridos.</label>");
      },
      errorLabelContainer: $(".errorHolder")
    });
//=========================================================================================== 
    $('.form-reposicion').validate({
      rules: {
        oficina:{
          required: true
        },
        solicitante:{
          required: true
        }
      },
      messages: {
        oficina:{
          required: 'Campo requerido'
        },
        solicitante:{
          required: 'Campo requerido'
        }
      },
      submitHandler: function(form) {
        form.submit();
      },
      errorLabelContainer: $(".errorHolder")
    });
//=========================================================================================== 
    $('.form-registro').validate({
      rules: {
        oficina:{
          required: true
        },
        cliente:{
          required: true
        },
        tipo_doc:{
          required: true
        },
        doc:{
          required: true
        },
        tarjeta:{
          required: true
        },
        num_canjes:{
          required: true
        }
      },
      messages: {
        oficina:{
          required: 'Campo requerido'
        },
        cliente:{
          required: 'Campo requerido'
        },
        tipo_doc:{
          required: 'Campo requerido'
        },
        doc:{
          required: 'Campo requerido'
        },
        tarjeta:{
          required: 'Campo requerido'
        },
        num_canjes:{
          required: 'Campo requerido'
        }
      },
      submitHandler: function(form) {
        form.submit();
      },
      invalidHandler: function(e, validator) {
        $(".errorHolder").html("<label class='error'>Todos los campos son obligatorios.</label>");
      }
    });
//===========================================================================================     
    $('input, textarea').placeholder();
    $('.text').alpha({allow: " "});
    $('.number').numeric({ichars:",.-_;/*!ñÑ "});


});

