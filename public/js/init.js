require.config({
  paths: {
    'jquery':'../libs/jquery',
    'colorbox':'../libs/jquery.colorbox-min',
    'placeholder':'../libs/jquery.placeholder',
    'validation':'../modules/validation'
    
  },
  shim: {
    'validation':['jquery'],
    'colorbox':['jquery'],
    'placeholder':['jquery']
  }
});

require([
  'jquery',
  'validation',
  'placeholder',
  'colorbox'
],

function( $, validation ){

  validation($('#frm-data'));
  $('input, textarea').placeholder();

  $(".lnk").colorbox({
    inline:true, 
    innerWidth:"405px", 
    innerHeight:"380px", 
    onOpen:function(){
      //$('body').css('overflow','hidden');
    },
    onClosed:function(){
      //$('body').css('overflow','auto');
    }
  });

});
