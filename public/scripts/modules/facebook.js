define([
  'jquery'
], function($){

  function facebook($item){

    $item.click(function(event){
      event.preventDefault();
      
      var img = "http://prod.beneficios.nescafe.com.pe/static/images/compartir.jpg",
          setText = "texto a compartir",
          title = "titulo a compartir";

      FB.ui({
        method: 'feed',
        name: title,
        link: 'http://www.nescafe.com.pe/21_dias_de_buenos_habitos.axcms',
        picture: img,
        description: setText,
        message: ''
      });
      return false;

    });
    
    
  }
  return facebook;
});
