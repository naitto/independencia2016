define([
  'jquery'
], function($){
  function scrollTop($item){
    
    $item.on('click', bodyTop);

    function bodyTop(event){
      event.preventDefault();
      $('html, body').animate({scrollTop: 0}, 1000);
    }
    
    $(window).scroll(function(){
      var WH = $(window).height(),
          footer = $('footer').height();
          SH = $('body')[0].scrollHeight;

      if ( $('body').scrollTop() >= SH-WH-footer ) {
        $item.delay(25).fadeIn();
      }else{
        $item.delay(25).fadeOut();
      };
    });

  }
  return scrollTop;
});
