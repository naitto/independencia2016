require.config({
  paths: {
    'colorbox': 'libs/jquery.colorbox'
  }
});

define([
  'jquery',
  'colorbox'
], function($){

  function modales($modal){
    $.colorbox({inline:true, href:$modal, innerHeight:'100%', innerWidth:'100%', fixed:true});
  }
  return modales;
});
