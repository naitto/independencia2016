require.config({
  paths: {
    'jquery': '../libs/jquery',
    'jquery.validate': '../libs/jquery.validate',
    'alphanumeric': '../libs/jquery.alphanumeric'
  },
    shim: {
    'jquery.validate':['jquery'],
    'alphanumeric':['jquery']
  }
});

define([
  'jquery',
  'jquery.validate',
  'alphanumeric'
], function($){
  
  function validation($form){
    var valid = $form.attr('data-valid');
    $form.validate({
      rules: {
        centro:{
          required: true
        },
        nombre:{
          required: true
        },
        apellido:{
          required: true
        },
        tipo_doc:{
          required: true
        },
        dni:{
          required: true,
          //number: true,
          minlength: 7,
          remote: {
            url: valid,
            type: "post",
            data: {
              dni: function() {
                return $( "#dni" ).val();
              },
              action: function() {
                return $( "#action" ).val();
              }
            }
          }
        },
        celular:{
          required: true,
          accept: "[9]+",
          minlength: 9
        },
        email:{
          required: true,
          email:true
        },
        tyc:{
          required:true
        },
        action:{
          required:true
        }
      },
      messages: {
        centro:{
          required: 'error'
        },
        nombre:{
          required: 'error'
        },
        apellido:{
          required: 'error'
        },
        tipo_doc:{
          required: 'error'
        },
        dni:{
          required: '',
          number:'error',
          minlength: '',
          remote:'Tu documento ya se encuentra registrado.'
        },
        celular:{
          required: '',
          accept : 'El número de celular debe comenzar con 9'
        },
        email:{
          required: 'error',
          email:'error'
        },
        tyc:{
          required: 'error'
        },
        action:{
          required: 'error'
        }
      },
      submitHandler: function(form) {
        //$(form).ajaxSubmit();
        var url = $(form).attr('action');
        var urlGracias = $(form).attr("data-end");
        var vista = $( "#action" ).val();
        $('.veladura').css('display','block');
        /*$('.btn-enviar').attr( "disabled", "disabled");*/
        $.ajax({
          url: url,
          type: 'post',
          data: $(form).serialize({ checkboxUncheckedValue: false }),
          success: function(response) {
            
            if (response == 1) {

              /*var dataLayer = window.dataLayer || [];
              window.dataLayer.push({
               'event': 'atm.formSubmit',
               'result': 'Success'
              });

*/
              location.href = urlGracias;
              /*$('form, .veladura').css('display','none');
              $('.text-mas').css('display','none');

              //$('h1').html('¡Felicidades!');
              $('.nombre').html($('#nombre').val());
              $('.gracias').css('display','block');*/


            }else{
              /*var dataLayer = window.dataLayer || [];
              window.dataLayer.push({
               'event': 'atm.formSubmit',
               'result': 'Failure'
              });*/

            };
          }            
        });

        //console.log('envio');

        //return false;  //This doesn't prevent the form from submitting.
      },
      invalidHandler: function(e, validator) {
        // $(".box-error").css('display','block').html("Debes completar o ingresar los datos correctos en los siguientes campos");
      },
      errorPlacement: function(error, element) {
        error.insertBefore(element);
      }
    });

    $('.text').alpha({allow: " "});
    $('.number').numeric({ichars:"ñÑ ,.:-_¿'*/+%$&"});

    jQuery.validator.addMethod("accept", function(value, element, param) {
      return value.match(new RegExp('^[9]'));
    });

  }
  return validation;
});
