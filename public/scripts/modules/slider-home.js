require.config({
  paths: {
    'jquery': '../libs/jquery',
    'flexslider':'../libs/jquery.flexslider-min'
  },
    shim: {
    'flexslider':['jquery']
  }
});

define([
  'jquery',
  'flexslider'
], function($){
  function sliderHome($slider, $thumb){
    
    //slider home
    $slider.flexslider({
      animation: "slide",
      /*controlNav: true,
      directionNav:true,
      animationLoop: true,*/
      slideshow: false,
      sync: $thumb
    });

    // thumb home
    $thumb.flexslider({
      animation: "slide",
      controlNav: false,
      directionNav:false,
      animationLoop: false,
      slideshow: true,
      itemWidth: 310,
      itemMargin: 5,
      asNavFor: $slider,
      move:1
    });

  }
  return sliderHome;
});
