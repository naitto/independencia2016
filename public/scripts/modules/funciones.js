require.config({
  paths: {
    'jquery': '../libs/jquery'
  },
    shim: {

  }
});

define([
  'jquery'
], function($){
    
    $('#action').change(function(){

      //console.log($(this).find(':selected').attr('data-url'));
      var saveUrl = $( this ).find( ':selected' ).attr( 'data-url' );
      $( '#frm-data' ).attr('action',saveUrl);
      $( '.frm-cards' ).find( 'input, select' ).prop( "disabled", false )
                                               .removeClass( 'disabled' );

      if ($(this).val()=='prepago') {
        $('#num-tarjeta').css('display','block');
      }else{
        $('#num-tarjeta').css('display','none');
      }

    });

    function updateText(event){
      var input=$(this);
      setTimeout(function(){
        var val=input.val();
        if(val!="")
          input.parent().addClass("floating-placeholder-float");
        else
          input.parent().removeClass("floating-placeholder-float");
      },1)
    }

    $(".floating-placeholder input").keydown(updateText);
    $(".floating-placeholder input").change(updateText);

});
