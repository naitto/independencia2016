define([
  'jquery'
], function($){

  function twitter($item){

    $item.click(function(event){
      event.preventDefault();
      var parent = $(this).closest('.col'),
          category = $(this).attr('data-category'),
          setText = "texto a compartir en twitter",
          urlLink = "http://bit.ly/BuenosHabitos",
          popWwidth = 570,
          popHeight = 280,
          left = (screen.width/2)-(popWwidth/2),
          top = (screen.height/2)-(popHeight/2),
          url = encodeURI('https://twitter.com/intent/tweet?text='+setText);
      
      window.open(url, 'Twitter', "width="+popWwidth+",height="+popHeight+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+left+",top="+top+"");
    
    });
  }
  return twitter;
});
