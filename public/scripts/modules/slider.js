require.config({
  paths: {
    'jquery': '../libs/jquery',
    'flexslider':'../libs/jquery.flexslider-min'
  },
    shim: {
    'flexslider':['jquery']
  }
});

define([
  'jquery',
  'flexslider'
], function($){
  function slider($slider, width, items){
    
    $slider.flexslider({
      animation: "slide",
      controlNav: false,
      directionNav:true,
      animationLoop: false,
      slideshow: false,
      itemWidth: width,
      itemMargin: items,
      move:1
    });

  }
  return slider;
});
