require.config({
  paths: {
    'jquery':'../libs/jquery',
    'colorbox':'../libs/jquery.colorbox-min'
  },
  shim: {
    'colorbox':['jquery']
  }
});

require([
  'jquery',

  'colorbox'
],

function( $ ){

  $(".layer").colorbox({inline:true, innerWidth:"441px", innerHeight:"700px"});
  $(".btn-url").colorbox({iframe:true, innerWidth:"451px", innerHeight:"681px"});

});
