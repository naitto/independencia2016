require.config({
  paths: {
    'jquery':'../libs/jquery',
    'colorbox':'../libs/jquery.colorbox-min',
    'placeholder':'../libs/jquery.placeholder',
    'validation':'../modules/validation',
    'funciones':'../modules/funciones'
    
  },
  shim: {
    'placeholder':['jquery'],
    'colorbox':['jquery'],
    'validation':['jquery'],
    'funciones':['jquery']
  }
});

require([
  'jquery',
  'validation',
  'placeholder',
  'colorbox',
  'funciones'
],

function( $, validation ){

  //$('.frm-cards').find('input, select').prop( "disabled", true )
  //                                     .addClass( 'disabled' );

/*  $('.frm-cards').find('#action').prop( "disabled", false )
                                 .removeClass( 'disabled' );*/
  
  validation($('#frm-data'));

  $('#tipo_doc').change(function(){
    $('#dni').val('');
    $('#dni').focus();
  });

  $(".lnk-tyc").colorbox({
    inline:true,
    //href:'#terminos', 
    innerWidth:"441px", 
    maxWidth:"95%", 
    innerHeight:"480px",
    onOpen:function(){
      //$('body').css('overflow','hidden');
    }
  });

  $('#tipo_doc').click(function(){
    $('input, select').prop( "disabled", false );
    $('input, select').css('border','1px solid #84c6ee');
  });

});
