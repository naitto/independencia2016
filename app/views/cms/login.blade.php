<!doctype html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta charset="UTF-8">
  <title>CMS BBVA</title>
  {{ HTML::style('css/cms/reset.css') }}
  {{ HTML::style('css/cms/style.css') }}
</head>
<body>
  <div class="wrapper clearfix">
    <div class="header">
      <div class="logo"></div>
    </div>
    
    <div class="section">

        <div id="ingreso">
          <div class="title">
            <h1>Administrador</h1>
          </div>

          @if(Session::get('msg'))
              <p class='errorLogin'>{{ Session::get('msg') }}</p>
          @endif
          <div class='errorHolder'></div>
          {{ Form::open(array('url' => 'login', 'id'=>'access', 'class'=>'form-login')) }}
            
            <!-- if there are login errors, show them here -->
            @if (Session::get('loginError'))
              <div class="alert alert-danger">{{ Session::get('loginError') }}</div>
            @endif

            <p>
              {{ $errors->first('username') }}
              {{ $errors->first('password') }}
            </p>

            {{ Form::text('username', Input::old('username'), array('placeholder' => 'Usuario', 'class' => 'left')) }}
            {{ Form::password('password', array('placeholder' => 'Clave', 'class' => 'right')) }}
            <div class="clear"></div>
            {{ Form::submit('Ingresar',array('class' => 'btn-ingresar btn-in')) }}
            
          {{ Form::close() }}
        </div>
    </div>
  </div>
  {{ HTML::script('js/jquery.js') }}
  {{ HTML::script('js/jquery.validate.js') }}
  {{ HTML::script('js/jquery.alphanumeric.js') }}
  {{ HTML::script('js/jquery.placeholder.js') }}
  {{ HTML::script('js/validation.js') }}
</body>
</html>