<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1, maximum-scale=1.0, user-scalable=false">
    <title>
    </title>
    <meta name="description" content="">
    {{ HTML::style('css/styles.css') }}
    {{ HTML::style('css/colorbox.css') }}
  </head>
  <body class='overlayTerminos'>

    <div class="alerta-ie8">
      El navegador que está usando no soporta toda la funcionalidad de la web, por favor actualice su navegador a una versión superior. {{ HTML::image("img/ico-info-obsoleto.png") }}
      <a href="#" class="btn-cerrar-aviso"></a>
    </div>

    <div class="veladura">
      <div class="procesando">
        <p>Procesando...</p>
      </div>
    </div>
    
    <h1>
      <img src="img/header-mobile.jpg" alt="" class="imgfix header-mobile">
    </h1>  

    <h2><span class="color-blue">Este 26 y 27 de abril realiza compras online con tus Tarjetas de Crédito BBVA Continental y BBVA Continental LifeMiles.</span><b class="color-cyan">Y recibirás un bono de 1000 Puntos Vida <br />o 1000 Millas LifeMiles.</b></h2>
    
    <div id='credito'>
      <div class="clearfix">
        <div class="banner">
          <img src="img/banner.jpg" alt="" class="imgfix header-web">  
        </div>
        <div class='form'>
          <div class="wrapper">
            <p class="text-mas">
              <span class="color-blue">Este 26 y 27 de abril realiza compras online con tus Tarjetas de Crédito BBVA Continental y BBVA Continental LifeMiles.</span><b class="color-cyan">Y recibirás un bono de 1000 Puntos Vida <br />o 1000 Millas LifeMiles.</b>
            </p>
            <p class="clear">Regístrate aquí y gana engriendo a mamá.</p>

            <form name='frm-data' id='frm-data' method="post" action="{{ URL::to('credito') }}" data-valid='{{ URL::to('valid') }}' data-end='{{ URL::to('gracias') }}'>    
          
              <select name='tipo_doc' id='tipo_doc' >
                <option value="">Tipo de documento *</option>
                <option value="L">DNI</option>
                <option value="P">PASAPORTE</option>
                <option value="E">CARNÉ DE EXTRANJERIA</option>
                <option value="D">CARNÉ DE DIPLOMÁTICO</option>
                <option value="M">CARNÉ MILITAR</option>
              </select>
              <div class="floating-placeholder" id='numero-doc'>
                <input type='text' id='dni' name='dni' maxlength="11" onpaste="return false;" disabled> 
                <label for="nombre" class="placeholder">Número de documento*</label>
              </div>
              <div class="floating-placeholder">
                <input type='text' id='nombre' name='nombre' class='text' onpaste="return false;" disabled>
                <label for="nombre" class="placeholder">Nombre*</label>
              </div>
            
              <div class="floating-placeholder">
                <input type='text' id='apellido' name='apellido' class="text" onpaste="return false;" disabled>
                <label for="apellido" class="placeholder">Apellido*</label>
              </div>

              <div class="floating-placeholder">
                <input type='text' id='email' name='email' onpaste="return false;" disabled >
                <label for="email" class="placeholder">Correo electrónico*</label>
              </div>

              <div class="floating-placeholder" id="numero-celular">
                <input type='text' id='celular' name='celular' onpaste="return false;" class="number" maxlength="9" disabled>
                <label for="celular" class="placeholder">Celular*</label>
              </div>

              <div class="checkbox">
                <input type="hidden" name="tyc" value="N"/>
                <input type='checkbox' id='tyc' name='tyc' value="S" disabled><a href='#terminos' class='lnk lnk-tyc' >He leído y acepto los Términos y Condiciones.<sup>(1)</sup></a><br>

                <input type="hidden" name="tdd" value="N"/>
                <input type='checkbox' id='tdd' name='tdd' value="S" disabled><a href='#personales' class='lnk lnk-tyc' >He leído y autorizo el Tratamiento de Datos Personales</a>
              </div>
            
              <input type='hidden' name='action' id='action' value='credito'>

              <br><br>
              <button class='btn-enviar' onClick="_gaq.push(['_trackEvent', 'gracias', 'formulario'])">ENVIAR</button>
              <p>Conoce más de esta campaña en <br><a href="https://www.bbvacontinental.pe/personas/tarjetas/multiplica.jsp" target="_blank">bbvacontinental.pe</a>*</p>

              <p class="direccion"><b>BBVA Banco Continental S.A. 2016 </b>- Teléfono: (01) 595-0000 - <br>Website: bbvacontinental.pe - Dirección: Avenida República de <br> Panama Nro. 3055 <br>San Isidro - Lima  </p>
            
            </form>

          </div><!--wrapper-->
        </div>
      </div>

      <footer class="terminos">
        <div class="capsula ">
          
          <div class="capsula__txt hide">
            <div class="capsula__img">
              <img src="img/capsula.jpg">
            </div> 
            <br>
          </div>
        </div>

        <div class="text-legal-box clearfix" style="height:auto;">
            <span>Términos y Condiciones</span>
            <br><br>

          <span>Tarjetas de Crédito PUNTOS VIDA:</span><br>
          Bono de 1,000 Puntos Vida por compras acumuladas a partir de S/.500, válida sólo para compras por Internet en los comercios online participantes del 26 al 27 de abril de 2016, siempre que se realice el pago total con Tarjetas de Crédito del BBVA Continental Visa: Clásica, Oro, Platinum, Signature; y MasterCard: Clásica, Oro, Platinum y Black. Información sobre comercios online participantes en bbvacontinental.pe. Para participar de la campaña debes inscribirte en bbvacontinental.pe. La inscripción puede ser realizada como máximo hasta el 27 de abril de 2016, incluso después de haber realizado los consumos.  Esta campaña no es acumulable con otras promociones. No válida para compras rechazadas por el establecimiento.  El abono de Puntos Vida se verá reflejado en el Estado de Cuenta del 10 de Junio de 2016 para Tarjetas Visa y 20 de Junio de 2016 para Tarjetas MasterCard. Aplica un solo bono por cliente titular durante toda la promoción, así haya realizado compras con más de una Tarjeta de Crédito. En caso cuente con Tarjeta de Crédito Puntos Vida y LifeMiles el bono se considerará por la tarjeta donde tenga el mayor consumo o cantidad de consumos. Las compras deben ser efectivas dentro del plazo de vigencia de la presente campaña (no válida para compras rechazadas por el establecimiento) y que la transacción se haya realizado en su totalidad en la misma página del comercio por internet (no válida para reservas por internet, pagos en nuestra Banca por Internet, compras por teléfono ni compras contra entrega). No participan Tarjetas que se encuentren bloqueadas por mora o sobregiro, al momento de realizar el abono de puntos. No participan Tarjetas de Crédito Nacional, Zero, Repsol, Capital de Trabajo, Empresariales ni Tarjetas de Débito. No válido para disposición de efectivo, subrogación de deuda, consumos en casinos, ni pagos SUNAT, SAT o Centros Educativos como Universidades, Colegios, etc.  No acumulan puntos los consumos en casinos, las disposiciones de efectivo, traslados de deuda y los pagos realizados a través de Banca por Internet.
          <br><br>
          <span>Tarjetas de Crédito LIFEMILES:</span><br>
          Bono de 1,000 millas por compras acumuladas desde S/.500 a más, válida sólo para compras por Internet en los comercios online participantes del 26 al 27 de abril de 2016, siempre que se realice el pago total con Tarjetas de Crédito del BBVA Continental LifeMiles Oro, LifeMiles Platinum, LifeMiles Signature. Información sobre comercios online participantes en bbvacontinental.pe y en http://cyberdays.pe. Para participar de la campaña debes inscribirte en bbvacontinental.pe. La inscripción puede ser realizada como máximo hasta el 27 de abril de 2016, incluso después de haber realizado los consumos. Esta campaña no es acumulable con otras promociones. Las millas se verán reflejadas en su cuenta de LifeMiles el 17 de Junio de 2016.  Aplica un solo bono por cliente titular durante toda la promoción, así haya realizado compras con más de una Tarjeta de Crédito. LifeMiles B.V. no es responsable del desarrollo de la presente campaña. Únicamente es responsable de la acreditación de las millas en la cuenta del socio de acuerdo a los reportes de BBVA Continental. En caso cuente con Tarjeta de Crédito Puntos Vida y LifeMiles el bono se considerará por la tarjeta donde tenga el mayor consumo o cantidad de consumos.  Las compras deben ser efectivas dentro del plazo de vigencia de la presente campaña (no válida para compras rechazadas por el establecimiento) y que la transacción se haya realizado en su totalidad en la misma página del comercio por internet (no válida para reservas por internet, pagos en nuestra Banca por Internet, compras por teléfono ni compras contra entrega). No participan Tarjetas de Crédito Nacional, Zero, Repsol, Capital de Trabajo, Empresariales ni Tarjetas de Débito. No participan Tarjetas que se encuentren bloqueadas por mora o sobregiro, al momento de realizar el abono de millas. No válido para disposición de efectivo, subrogación de deuda, consumos en casinos, ni pagos SUNAT, SAT o Centros Educativos como Universidades, Colegios, etc. No acumulan millas los consumos en casinos, las disposiciones de efectivo, traslados de deuda y los pagos realizados a través de Banca por Internet. Otras condiciones de acumulación de las tarjetas de marca compartida están en el reglamento de beneficios de las tarjetas de crédito BBVA Continental LifeMiles que se encuentra en bbvacontinental.pe. El cliente debe aceptar los términos y condiciones del Programa y la Política de Privacidad de LifeMiles ingresando a lifemiles.com, de lo contrario podría no tener la posibilidad de redimir sus millas y/o no recibir información del Programa LifeMiles. LifeMiles es una marca registrada de LifeMiles B.V. A las Millas acumuladas les son aplicables los términos y condiciones del Programa LifeMiles. Consúltalos en LifeMiles.com.

        </div>
        <div class="clearfix"></div>
      </footer>


      <div style='display:none;'>
        <div id='terminos' class='terminos'>
          <span class="title-legal-box">He leído las condiciones de la promoción</span>
          <br><br>
          <div class="text-legal-box">
            <span>Tarjetas de Crédito PUNTOS VIDA:</span><br>
            Bono de 1,000 Puntos Vida por compras acumuladas a partir de S/.500, válida sólo para compras por Internet en los comercios online participantes del 26 al 27 de abril de 2016, siempre que se realice el pago total con Tarjetas de Crédito del BBVA Continental Visa: Clásica, Oro, Platinum, Signature; y MasterCard: Clásica, Oro, Platinum y Black. Información sobre comercios online participantes en bbvacontinental.pe. Para participar de la campaña debes inscribirte en bbvacontinental.pe. La inscripción puede ser realizada como máximo hasta el 27 de abril de 2016, incluso después de haber realizado los consumos.  Esta campaña no es acumulable con otras promociones. No válida para compras rechazadas por el establecimiento.  El abono de Puntos Vida se verá reflejado en el Estado de Cuenta del 10 de Junio de 2016 para Tarjetas Visa y 20 de Junio de 2016 para Tarjetas MasterCard. Aplica un solo bono por cliente titular durante toda la promoción, así haya realizado compras con más de una Tarjeta de Crédito. En caso cuente con Tarjeta de Crédito Puntos Vida y LifeMiles el bono se considerará por la tarjeta donde tenga el mayor consumo o cantidad de consumos. Las compras deben ser efectivas dentro del plazo de vigencia de la presente campaña (no válida para compras rechazadas por el establecimiento) y que la transacción se haya realizado en su totalidad en la misma página del comercio por internet (no válida para reservas por internet, pagos en nuestra Banca por Internet, compras por teléfono ni compras contra entrega). No participan Tarjetas que se encuentren bloqueadas por mora o sobregiro, al momento de realizar el abono de puntos. No participan Tarjetas de Crédito Nacional, Zero, Repsol, Capital de Trabajo, Empresariales ni Tarjetas de Débito. No válido para disposición de efectivo, subrogación de deuda, consumos en casinos, ni pagos SUNAT, SAT o Centros Educativos como Universidades, Colegios, etc.  No acumulan puntos los consumos en casinos, las disposiciones de efectivo, traslados de deuda y los pagos realizados a través de Banca por Internet.
            <br><br>
            <span>Tarjetas de Crédito LIFEMILES:</span><br>
            Bono de 1,000 millas por compras acumuladas desde S/.500 a más, válida sólo para compras por Internet en los comercios online participantes del 26 al 27 de abril de 2016, siempre que se realice el pago total con Tarjetas de Crédito del BBVA Continental LifeMiles Oro, LifeMiles Platinum, LifeMiles Signature. Información sobre comercios online participantes en bbvacontinental.pe y en http://cyberdays.pe. Para participar de la campaña debes inscribirte en bbvacontinental.pe. La inscripción puede ser realizada como máximo hasta el 27 de abril de 2016, incluso después de haber realizado los consumos. Esta campaña no es acumulable con otras promociones. Las millas se verán reflejadas en su cuenta de LifeMiles el 17 de Junio de 2016.  Aplica un solo bono por cliente titular durante toda la promoción, así haya realizado compras con más de una Tarjeta de Crédito. LifeMiles B.V. no es responsable del desarrollo de la presente campaña. Únicamente es responsable de la acreditación de las millas en la cuenta del socio de acuerdo a los reportes de BBVA Continental. En caso cuente con Tarjeta de Crédito Puntos Vida y LifeMiles el bono se considerará por la tarjeta donde tenga el mayor consumo o cantidad de consumos.  Las compras deben ser efectivas dentro del plazo de vigencia de la presente campaña (no válida para compras rechazadas por el establecimiento) y que la transacción se haya realizado en su totalidad en la misma página del comercio por internet (no válida para reservas por internet, pagos en nuestra Banca por Internet, compras por teléfono ni compras contra entrega). No participan Tarjetas de Crédito Nacional, Zero, Repsol, Capital de Trabajo, Empresariales ni Tarjetas de Débito. No participan Tarjetas que se encuentren bloqueadas por mora o sobregiro, al momento de realizar el abono de millas. No válido para disposición de efectivo, subrogación de deuda, consumos en casinos, ni pagos SUNAT, SAT o Centros Educativos como Universidades, Colegios, etc. No acumulan millas los consumos en casinos, las disposiciones de efectivo, traslados de deuda y los pagos realizados a través de Banca por Internet. Otras condiciones de acumulación de las tarjetas de marca compartida están en el reglamento de beneficios de las tarjetas de crédito BBVA Continental LifeMiles que se encuentra en bbvacontinental.pe. El cliente debe aceptar los términos y condiciones del Programa y la Política de Privacidad de LifeMiles ingresando a lifemiles.com, de lo contrario podría no tener la posibilidad de redimir sus millas y/o no recibir información del Programa LifeMiles. LifeMiles es una marca registrada de LifeMiles B.V. A las Millas acumuladas les son aplicables los términos y condiciones del Programa LifeMiles. Consúltalos en LifeMiles.com.

          </div>
        </div>

        <div id='personales' class='terminos'>
          <span class="title-legal-box">Permítenos estar más cerca de ti</span>
          <br><br>
          <div class="text-legal-box">
            Permítenos tratar los datos personales que nos dejes en este formulario, a nosotros y nuestras empresas vinculadas, directamente o a través de proveedores, para enviarte publicidad, hacerte encuestas, invitaciones, ofertas  y/o evaluar darte productos y/o servicios del Banco, nuestras vinculadas y/o de terceros. Tus datos serán parte de una base de datos del Banco de duración indefinida y podrá ser  tratada  y/o ampliada, en el país como en el exterior, por el Banco, sus vinculadas y/o proveedores. <br>
            Si no estás de acuerdo, no podremos hacer lo indicado. Si estás de acuerdo y luego ya no quieres, puedes revocarla y/o ejercer los otros derechos que la ley te da, en cualquiera de nuestras oficinas u otro canal que creemos en el banco, con sede en Av. Rep. de Panamá 3055 San Isidro, Lima. 
          </div>
        </div>

      </div>
    </div>
    <script>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-59439143-1']);
      _gaq.push(['_trackPageview']);
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

    <?php
      $url = url('scripts/');
      echo "<script data-main='".$url."/site/form' src='".$url."/libs/require.js'></script>";
    ?>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '254908304633078');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Code for Remarketing Tag -->
    <!-- 
    Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
     -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 947361018;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/947361018/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

  </body>
</html>