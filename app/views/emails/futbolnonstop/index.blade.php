<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
  
         <table width="600" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="40" style="font-weight: normal; font-size: 10px; font-family: Verdana, Helvetica, sans-serif; line-height: 12px; color: #004c93; text-align: center;">Para asegurar la entrega de nuestros e-mail en su correo, <br />
      por favor agregue p r o m o c i o n e s @ g r u p o b b v a . c o m . p e</a> a su libreta de direcciones de correo. <br /></td>
    </tr>
    <tr>
          <td>&nbsp;</td>
    </tr>
    <tr>
      <td><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image1.jpg" width="600" height="126" border="0" style="display: block; vertical-align: bottom;" /></td>
    </tr>
    
        
        <tr>
      <td>
            <table bg="#ffffff;" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#ffffff" width="42"height="21"></td>
                <td bgcolor="#ffffff" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1em; color: #004c93; font-weight: normal; text-align:left;"><span style="color: #c5206c;">Hola {{ $nombre }},</span></td>
               
              </tr>
            </table>
      </td>
    </tr>
        <tr><td height="95"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image2.jpg" width="600" height="419"  border="0" style="display: block; vertical-align: bottom;" /></td></tr>
        
         <tr><td ><table width="515" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td align="center" valign="middle" style="font-size:47px; color:#18a6e4; padding:5px 0;">
    
      <p style="margin:0;"><strong>{{ $centro }}</strong></p></td>
  </tr>
  
</table>
</td></tr>
       
       <tr>
         <td height="81"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image3.jpg" width="600" height="81"  border="0" style="display: block; vertical-align: bottom;" /></td></tr> 
        


<tr>
      <td valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="127" valign="top"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image-7.png" width="43" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="129"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/facebook.jpg" width="76" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="114"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/twitter.jpg" width="80" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/youtube.jpg" width="94" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/instagram.jpg" width="89" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/googleplus.jpg" width="100" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/linkedin.jpg" width="77" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="110"><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image-12.jpg" width="41" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
  </tr>
</table>
</td>
    </tr>
<tr><td><img src="http://serviciosbbvaperu.com/mailings/2015/junio/30-06-15-agradecimiento-futbolnonstop/imagenes/image13.jpg" width="600" height="100" border="0" style="display: block; vertical-align: bottom;" /></td></tr>


      
      <tr>
    <td colspan="2" align="center" valign="top"><table width="515" border="0" cellspacing="0" cellpadding="0">
    
      <tr></tr>
     
      <tr>
        <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: #67666a; text-align: left; line-height: 11px; padding: 20px 0px; text-align: justify;"><p>
        <span style="color:#004c93;">T&eacute;rminos y condiciones:</span>
            <br>
            Promoci&oacute;n v&aacute;lida del 01/07/15 al 30/08/15 con las Tarjetas de Cr&eacute;dito y D&eacute;bito del BBVA Continental. Se sortear&aacute;n 6 experiencias dobles para ver el cl&aacute;sico de la Liga BBVA de la temporada 2015-2016, Real Madrid vs Barcelona, 4 experiencias dobles para opciones con Tarjetas de Cr&eacute;dito y 2 experiencias dobles para opciones con Tarjetas de D&eacute;bito.
Para entrar al sorteo acumular&aacute;n opciones por las compras con Tarjetas de Cr&eacute;dito o D&eacute;bito en establecimientos (POS), por internet, por tel&eacute;fono, compras nacionales e internacionales (no hay monto m&iacute;nimo de compra), se acumular&aacute; de la siguiente manera (1 gol = 1 opci&oacute;n):
 - Las compras con Tarjetas de Cr&eacute;dito: 2 GOLES. Si la compra fue en CUOTAS: gana 4 GOLES M&Aacute;S y adem&aacute;s MULTIPLICA X2 sus Puntos Vida o Millas.<br>
 - Las compras con Tarjetas de D&eacute;bito: 1 GOL.<br>
El sorteo se realizar&aacute; el d&iacute;a 08/09/2015.<br>
Estas experiencias  comprenden:<br>

- Boleto a&eacute;reo ida y vuelta en clase turista desde Lima a Madrid/Barcelona (la locaci&oacute;n donde se defina el partido).<br>
- Alojamiento en un hotel 4*, 4 noches en habitaci&oacute;n doble.<br>
- Traslados dentro de Madrid/Barcelona (la locaci&oacute;n donde se defina el partido).<br>
- Todos los servicios incluidos en la experiencia (desayunos, almuerzos, cenas, visitas y actividades).<br>
- Entradas al partido Real Madrid vs Barcelona.<br>
- Seguro de viaje.<br><br>
Adem&aacute;s, se sortear&aacute;n 2&acute;000,000 de Puntos Vida o Millas de la siguiente manera (en caso el ganador cuente con Tarjeta Puntos Vida y LifeMiles el premio se le abonar&aacute; s&oacute;lo a una opci&oacute;n entre Puntos o Millas, y ser&aacute; a elecci&oacute;n del cliente):<br>
- 4 premios de 100,000 puntos vida o millas<br>
- 10 premios de 50,000 puntos vida o millas<br>
- 20 premios de 20,000 puntos vida o millas<br>
- 30 premios de 10,000 puntos vida o millas<br>
- 40 premios de 5,000 puntos vida o millas<br>
- 200 premios de 1,000 puntos vida o millas<br><br>
Para participar de la promoci&oacute;n debes inscribirte ingresando a b b v a c o n t i n e n t a l . p e en el bot&oacute;n de la promoci&oacute;n o en Banca por Tel&eacute;fono llamando al (01) 595-0000 Lima o al 0800-101-08 Provincias en la Opci&oacute;n 2, 7 hasta el 30/08/2015 a&uacute;n despu&eacute;s de haber realizado sus consumos.<br><br>
Participan las Tarjetas de Cr&eacute;dito Visa: Nacional, Zero, Repsol, Cl&aacute;sica, Oro, Platinum, Signature, LifeMiles Oro, LifeMiles Platinum, LifeMiles Signature; y,  MasterCard: Cl&aacute;sica, Oro, Platinum y Black. Participan las Tarjetas de D&eacute;bito Visa: Compras, D&eacute;bito VIP  y Mundo Sueldo.
No participan  Tarjetas de Cr&eacute;dito Capital de Trabajo, Empresariales ni Prepago.<br>
No participan Tarjetas que se encuentren bloqueadas por mora o sobregiro, al momento de realizar el sorteo. No v&aacute;lido para disposici&oacute;n de efectivo, subrogaci&oacute;n de deuda, consumos en casinos, compras en p&aacute;ginas web de juegos de azar, pagos en banca por internet, ni pagos de impuestos SUNAT, SAT. Esta campa&ntilde;a no es acumulable con otras promociones.<br>
Las compras realizadas por los adicionales, suman opciones para el titular.<br>
Si el cliente acumula opciones por Tarjeta de Cr&eacute;dito y adem&aacute;s por D&eacute;bito, el total de sus opciones valdr&aacute;n para el sorteo de Tarjetas de Cr&eacute;dito. V&aacute;lido s&oacute;lo un premio por cliente. Los premios ser&aacute;n usados exclusivamente para la experiencia de viajar a ver el partido, m&aacute;s no canjeado por dinero en efectivo. Los premios son intransferibles.<br>
Los ganadores ser&aacute;n informados telef&oacute;nicamente al n&uacute;mero que aparece registrado en la base de datos del Banco y a trav&eacute;s de nuestra p&aacute;gina web b b v a c o n t i n e n t a l . p  e . El cliente tiene hasta el 21/09/2015 para reclamar su premio, no habiendo posibilidad de reclamo posterior. Si hasta esa fecha el ganador no reclama su premio, este pasar&aacute; a un siguiente ganador de la lista de reserva. El ganador es el &uacute;nico responsable de los tr&aacute;mites de Visado Schengen. El banco no se responsabiliza por los gastos de traslados internos, si el ganador fuera de provincia.<br>
El premio podr&aacute; ser entregado en la principal del BBVA Continental o mediante un operador log&iacute;stico en la direcci&oacute;n que se tenga registrada en la Base de Datos del Banco. Adem&aacute;s, el operador log&iacute;stico coordinar&aacute; la fecha y hora de entrega del mismo. Si el tel&eacute;fono no estuviera actualizado, el Banco no asume responsabilidad por la imposibilidad de entrega del premio.<br>
<br>
RD: 2850-2015-ONAGI-DGAE-DA<br>
Informaci&oacute;n brindada seg&uacute;n Ley N&deg; 28587 y su Reglamento. Tasas de inter&eacute;s, comisiones, gastos y penalidades publicados en el Tarifario de Oficinas de Atenci&oacute;n al P&uacute;blico y en b b v a c o n t i n e n t a l . p e. Para mayor informaci&oacute;n, comun&iacute;cate al (01) 595-0000. ITF(0.005%) sujeto a variaci&oacute;n.
        
        
        <br><br><br>
        <span style="color:#004c93;">Nota:</span> Para garantizar la entrega de los emails a su bandeja de entrada, por favor a&ntilde;ada a su libreta de direcciones nuestra direcci&oacute;n p r o m o c i o n e s @ g r u p o b b v a . c o m . p e <br>
          <br>
          Si no desea que le lleguen nuestras promociones, por favor <u style="color:#004c93;"><a href="https://register.masterbase.com/v0/bbvape/#!serial!#/#!contrasena!#/marcarusu?boletin=9&habilitado=1&opcion=" target="_blank" style="color:#4553AA;">click aqu&iacute;</a></u>.<br>
          <br>
          Por tu seguridad, BBVA Continental te informa:<br>
          <br>
          &bull; Nunca solicitaremos tu datos confidenciales por correo, tales como la clave SMS de Internet, clave de cajero, DNI o tu n&uacute;mero de celular.<br>
          <br>
          &bull; Si tienes alguna duda acerca de la autenticidad de este correo env&iacute;alo a la direcci&oacute;n b b v a s e g u r i d a d . p e @ b b v a . c o m y te responderemos.</p></td>
      </tr>
    </table></td>
      </tr>
    <tr>
      <td colspan="2" height="15" style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: center;">
       <img src="http://www.google-analytics.com/collect?v=1&tid=UA-61613618-1&cid=email&t=event&ec=emailagradecimientofutbolnonstop&ea=open&el=emailing&cs=direct&cm=email&cn=062413&cm1=1" alt=""></td>
    </tr>
        
             
    
    
        
        <tr>
      <td colspan="2" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: center;">Este correo electr&oacute;nico ha sido enviado a {{ $email }} <br /></td>
  </tr>
      <tr>
        <td colspan="2"><hr size="1" color="#becccc" /></td>
      </tr>
    <tr>
      <td colspan="2" style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: left; padding-left: 45px;">Este correo electr&oacute;nico fue enviado por <strong>BBVA Continental</strong> <br />
Direcci&oacute;n: Av. Rep&uacute;blica de Panam&aacute; 3055 - San Isidro, Lima - Per&uacute; <br />
<strong>&copy; 2015 Derechos Reservados</strong></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
      </table>
     
  
  </td>
</tr>
</table>
</body>
</html>