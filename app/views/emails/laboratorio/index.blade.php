<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
         <table width="600" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="40" style="font-weight: normal; font-size: 10px; font-family: Verdana, Helvetica, sans-serif; line-height: 12px; color: #004c93; text-align: center;">Para asegurar la entrega de nuestros e-mail en su correo, <br />
      por favor agregue p r o m o c i o n e s @ g r u p o b b v a . c o m . p e</a> a su libreta de direcciones de correo. <br /></td>
    </tr>
    <tr>
          <td>&nbsp;</td>
    </tr>
    <tr>
      <td><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/image1.jpg" width="600" height="127" border="0" style="display: block; vertical-align: bottom;" /></td>
    </tr>
    
        
        <tr>
      <td>
            <table bg="#ffffff;" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#ffffff" width="42" height="21"></td>
                <td bgcolor="#ffffff" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1em; color: #004c93; font-weight: normal; text-align:left;"><span style="color: #c61c65;">Hola {{ $nombre }},</span></td>
               
              </tr>
            </table>
      </td>
    </tr>
        <tr><td height="438"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/image2.jpg" width="600" height="423" style="display: block; vertical-align: bottom;" border="0" /></td></tr>
<tr><td height="117"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/image3.jpg" width="600" height="133" style="display: block; vertical-align: bottom;" border="0" /></td></tr>




<tr>
      <td valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="127" valign="top"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/image-7.png" width="43" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="129"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/facebook.jpg" width="76" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="114"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/twitter.jpg" width="80" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/youtube.jpg" width="94" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/instagram.jpg" width="89" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/googleplus.jpg" width="100" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="120"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/linkedin.jpg" width="77" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
    <td width="110"><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/image-12.jpg" width="41" height="81" border="0" style="display: block; vertical-align: bottom;" /></td>
  </tr>
</table>
</td>
    </tr>
<tr><td><img src="https://serviciosbbvaperu.com/mailings/2016/abril/01-04-16-agradecimiento-cg2/imagenes/imagen13.jpg" width="600" border="0" style="display: block; vertical-align: bottom;" /></td></tr>



      
      <tr>
    <td colspan="2" align="center" valign="top"><table width="515" border="0" cellspacing="0" cellpadding="0">
    
    
      <tr>
        <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: #67666a; text-align: left; line-height: 11px; padding: 25px 0px; text-align: justify;"><p><span style="color:#004c93;">Nota:</span> Para garantizar la entrega de los emails a su bandeja de entrada, por favor a&ntilde;ada a su libreta de direcciones nuestra direcci&oacute;n p r o m o c i o n e s @ g r u p o b b v a . c o m . p e <br>
          <br>
          Si no desea que le lleguen nuestras promociones, por favor <u style="color:#004c93;"><a href="https://register.masterbase.com/v0/bbvape/#!serial!#/#!contrasena!#/marcarusu?boletin=9&habilitado=1&opcion=" target="_blank" style="color:#4553AA;">click aqu&iacute;</a></u>.<br>
          <br>
          Por tu seguridad, BBVA Continental te informa:<br>
          <br>
          &bull; Nunca solicitaremos tu datos confidenciales por correo, tales como la clave SMS de Internet, clave de cajero, DNI o tu n&uacute;mero de celular.<br>
          <br>
          &bull; Si tienes alguna duda acerca de la autenticidad de este correo env&iacute;alo a la direcci&oacute;n b b v a s e g u r i d a d . p e @ b b v a . c o m y te responderemos.</p></td>
      </tr>
      
      <tr>
      <td colspan="2" height="15" style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: center;">
      <img src="http://www.google-analytics.com/collect?v=1&tid=UA-61613618-1&cid=email&t=event&ec=email-cuentaganador2-laboratorio&ea=open&el=emailing&cs=direct&cm=email&cn=062413&cm1=1" alt="">
      </td>
    </tr>
    
    <tr>
      <td colspan="2" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: center;">Este correo electr&oacute;nico ha sido enviado a {{ $email }} <br /></td>
  </tr>
      <tr>
        <td colspan="2"><hr size="1" color="#becccc" /></td>
      </tr>
    <tr>
      <td colspan="2" style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: left;">Este correo electr&oacute;nico fue enviado por <strong>BBVA Continental</strong> <br />
Direcci&oacute;n: Av. Rep&uacute;blica de Panam&aacute; 3055 - San Isidro, Lima - Per&uacute; <br />
<strong>&copy; 2016 Derechos Reservados</strong></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    </table></td>
      </tr>
    
        
             
    
    
        
        
      </table></td>
  </tr>
    </table>    
  
  </td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>