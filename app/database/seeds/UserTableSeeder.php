<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		//DB::table('users')->delete();
		/*User::create(array(
			'name'     => 'BBVA',
			'username' => 'admin',
			'email'    => 'chris@scotch.io',
			'password' => Hash::make('administrador'),
			'remember_token' => ''
		));*/

		User::create(array(
			'name'     => 'BBVA',
			'username' => 'BBVA1',
			'role' => 'larco',
			'email'    => 'admin@bbvacontinental.pe',
			'password' => Hash::make('bbv4c0nt1n3nt4l'),
			'remember_token' => ''
		));

		User::create(array(
			'name'     => 'BBVA',
			'username' => 'BBVA2',
			'role' => 'larco',
			'email'    => 'admin@bbvacontinental.pe',
			'password' => Hash::make('l4rc0c0ntin3nt4l'),
			'remember_token' => ''
		));



	}

}