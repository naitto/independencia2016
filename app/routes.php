<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/independencia2016', array('uses' => 'HomeController@credito', 'before' => 'guest'));
Route::post('/credito', array('uses' => 'HomeController@creditoPost', 'before' => 'guest'));
Route::get('/gracias', array('uses' => 'HomeController@gracias', 'before' => 'guest'));
Route::post('/valid', array('uses' => 'HomeController@validDoc', 'before' => 'guest'));

/*ADMIN**/

Route::get('/login', array('uses' => 'HomeController@showLogin', 'before' => 'guest'));
Route::post('/login', array('uses' => 'HomeController@doLogin', 'before' => 'guest'));
Route::get('/logout', array('uses' => 'HomeController@doLogout', 'before' => 'auth'));

Route::group(array('before' => 'auth'), function()
{

    Route::get('/panel-independencia2016', array('uses' => 'HomeController@controlPanel'));
    Route::get('/exportarCreditoDATE', array('uses' => 'HomeController@exportarFechasCredito'));
    Route::get('/exportarCreditoXLS', array('uses' => 'HomeController@exportarUsuariosCredito'));

    Route::get('/logout', array('uses' => 'HomeController@doLogout'));
});