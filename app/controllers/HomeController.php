<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	protected $layout = 'cms/layout';
  /************************************************************************************/

  public function showLogin(){
    return View::make('cms/login');
  }

  public function controlPanel(){
    $totalCredito = DB::table('credito')->count();
    $fechasAgrupadas = DB::select( DB::raw("SELECT date_register,count(*) as total FROM credito GROUP BY date_register ASC"));

    //$totalDebito  = DB::table('debito')->count();
    //$totalPrepago = DB::table('prepago')->count();

    $data = array(
            'totalCredito' => $totalCredito,
            'fechasAgrupadas'  => $fechasAgrupadas
            //'totalPrepago' => $totalPrepago,
            );

    $roleUser = Auth::user()->username;
    $this->layout->content = View::make('cms/controlpanelAdmin')->with('totales',$data);
  }

  public function controlPanelMP(){
    $total = DB::table('creditomp')->count();
    $roleUser = Auth::user()->username;
    $this->layout->content = View::make('cms/controlpanelAdminMP')->with('total',$total);
  }

  public function doLogin(){
    $rules = array(
      'username'    => 'required',
      'password' => 'required|alphaNum|min:3'
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
      return Redirect::to('login')
        ->withErrors($validator) 
        ->withInput(Input::except('password')); 
    } else {
      $userdata = array(
        'username'  => Input::get('username'),
        'password'  => Input::get('password')
      );

      if (Auth::attempt($userdata)) {
        return Redirect::to('/panel-'. Auth::user()->role);
      } else {
        return Redirect::to('login');
      }

    }
  }

  public function doLogout(){
      Auth::logout();
      return Redirect::to('/login');
  }


  public function exportarFechasCredito(){

    $fechaInicio = Input::get('fecha_inicio').' 00:00:00';
    $fechaFin = Input::get('fecha_final').' 23:59:59';

    $results = DB::select( DB::raw("SELECT * FROM credito WHERE date_register>=:fechaInicio AND date_register <=:fechaFin ORDER BY date_register DESC"), array(
      'fechaInicio' => $fechaInicio, 'fechaFin' => $fechaFin
    ));

    $data = self::generarXLS($results,0,'USUARIOS_CAMPANIA_LABORATORIO_SECRETO');
    return Response::make($data[0], 200, $data[1]);
  }

  public function exportarUsuariosCredito(){
    $results = DB::table('credito')
                            ->orderBy('date_register', 'desc')
                            ->get();
    $data = self::generarXLS($results,0,'USUARIOS_CAMPANIA_LABORATORIO_SECRETO');
    return Response::make($data[0], 200, $data[1]);
  }


  function generarXLS($results, $card=0, $namedoc){
    $table = '';
    $cardValue='';
    $carHead='';

    foreach ($results as $key=>$user){
      if ($card!=0) {
        $cardValue = '<td>'. utf8_decode($user->num_card) .'</td>';
        $carHead ='<th>Numero de tarjeta</th>';
      }
      
      $table .= '<tr>
                <td>'. $user->tipo_doc .'</td>
                <td style="mso-number-format:\'@\'">'. $user->num_doc .'</td>
                <td>'. utf8_decode($user->nombre) .'</td>
                <td>'. utf8_decode($user->apellido) .'</td>
                <td>'. utf8_decode($user->celular) .'</td>'
                .$cardValue.
                '<td>'. utf8_decode($user->email) .'</td>
                <td>'. $user->terminos .'</td>
                <td>'. $user->tratamiento .'</td>
                <td style="mso-number-format:\'@\'">'. $user->date_register .'</td>
                <td style="mso-number-format:\'@\'">'. $user->hour_register .'</td>
                <td style="mso-number-format:\'@\'">'. $user->ip_address .'</td>
                </tr>';
    }

    $output = '<table border="1">
              <thead>
                <tr>
                  <th>Tipo documento</th>
                  <th>Num. documento</th>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Celular</th>
                  '.$carHead.'
                  <th>Email</th>
                  <th>Acepto terminos y condiciones</th>
                  <th>Acepto tratamiento de datos personales</th>
                  <th>Fecha de registro</th>
                  <th>Hora de registro</th>
                  <th>IP</th>
                </tr>
              </thead><tbody>'. $table .'</tbody></table>';

    $headers = array(
        'Pragma' => 'public',
        'Expires' => 'public',
        'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
        'Cache-Control' => 'private',
        'Content-Type' => 'application/vnd.ms-excel',
        'Content-Disposition' => 'attachment; filename='.$namedoc.'.xls',
        'Content-Transfer-Encoding' => ' binary'
    );

    return array($output, $headers);
  }


  /*********************************************************************************/

  public function gracias()
  {
    return View::make('gracias');
  }
  
  public function credito()
  {
    return View::make('credito');
  }

  public function creditoPost(){

    $validator = Validator::make(
      Input::all(),
      array(
        'tipo_doc'          => 'required',
        'dni'               => 'required',
        'nombre'            => 'required',
        'apellido'          => 'required',
        'email'             => 'required'
      ),
      array(
        'required'    => 'Estos datos son obligatorios',
        'numeric'     => 'Solo se permiten números',
        'email'       => 'Ingrese su correo electrónico'
      )
    );

    if ($validator->fails()) {
      return 'error';
    };
  
    DB::table('credito')->insert(array(
      'tipo_doc'            => Input::get('tipo_doc'),
      'num_doc'             => Input::get('dni'),
      'nombre'              => Input::get('nombre'),
      'apellido'            => Input::get('apellido'),
      'celular'             => Input::get('celular'),
      'email'               => Input::get('email'),
      'terminos'            => Input::get('tyc'),
      'tratamiento'         => Input::get('tdd'),
      'date_register'       => date("Y-m-d"),
      'hour_register'       => date('H:i:s'),
      'ip_address'          => $_SERVER['REMOTE_ADDR']
    ));


    /*
    Mail::send('emails.independencia2016.index',array('email'=>Input::get('email'), 'nombre'=>Input::get('nombre')), function($message){
       $message->to(Input::get('email'), Input::get('nombre'))->subject('Confirmación de inscripción');
    });
    */

    return 1;


  }
  /************************************************************************************/
  public function validDoc(){
    $numdoc = Input::get('dni');
    $action = Input::get('action');
    
    if($numdoc == null){
      return Redirect::to('/'.$action);
    }else{

      $results = DB::select( DB::raw("SELECT * FROM credito WHERE num_doc = :numdoc"), array('numdoc' => $numdoc));

      if( count($results) == 0 || $results=="" ){
        return 'true';
      }else{
        return 'false';
      }

    }
  }

/*  public function limpiarDebito(){
    $results = DB::table('credito')->get();
    DB::table('backup_credito')->insert(array(
      'registros'            => $results
    ));
  }*/


}